-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2013 at 01:11 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sourav`
--

-- --------------------------------------------------------

--
-- Table structure for table `bumpup`
--

CREATE TABLE IF NOT EXISTS `bumpup` (
  `email` varchar(50) NOT NULL,
  `amt` int(50) NOT NULL,
  `date` date NOT NULL,
  `expired` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE IF NOT EXISTS `industry` (
  `email` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `sex` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `location` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location`) VALUES
('Ludhiana'),
('Jalandhar'),
('Amritsar'),
('Bathinda');

-- --------------------------------------------------------

--
-- Table structure for table `registered_users`
--

CREATE TABLE IF NOT EXISTS `registered_users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_users`
--

INSERT INTO `registered_users` (`username`, `password`, `fname`, `lname`) VALUES
('iqbal@gmail.com', 'sandhu', 'Baljeet', 'Singh'),
('bsinghs.007@gmail.com', 'sandhu', 'Baljeet', 'Singh');

-- --------------------------------------------------------

--
-- Table structure for table `talents`
--

CREATE TABLE IF NOT EXISTS `talents` (
  `id` int(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `sex` text NOT NULL,
  `location` text NOT NULL,
  `image` varchar(150) NOT NULL,
  `imageThumb` varchar(150) NOT NULL,
  `audio` varchar(150) NOT NULL,
  `video` varchar(150) NOT NULL,
  `type` varchar(5) NOT NULL DEFAULT 'f',
  `who` varchar(15) NOT NULL DEFAULT 'talent',
  `credit` int(5) NOT NULL DEFAULT '0',
  `keyword` varchar(50) NOT NULL,
  `phone` int(10) NOT NULL,
  `age` int(10) NOT NULL,
  `category` varchar(50) NOT NULL,
  `subCategory` varchar(50) NOT NULL,
  `bumpup` varchar(50) NOT NULL,
  `updatedondate` datetime NOT NULL,
  `updatedontime` datetime NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `talents`
--

INSERT INTO `talents` (`id`, `email`, `password`, `sex`, `location`, `image`, `imageThumb`, `audio`, `video`, `type`, `who`, `credit`, `keyword`, `phone`, `age`, `category`, `subCategory`, `bumpup`, `updatedondate`, `updatedontime`) VALUES
(1, 'bsinghs.007@gmail.com', 'hello', 'male', 'ludhiana', 'http://localhost/phpimagemagic/upload/user28356.jpg', 'http://localhost/phpimagemagic/upload/thumbnail/user28356.jpg', 'http://localhost/phpimagemagic/upload/user28356.mp3', 'http://localhost/phpimagemagic/upload/user28356.mp4', 'f', 'talent', -5, '', 0, 0, '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'sandhusingh@gmail.com', 'siojfi', 'male', 'ludhiana', 'http://localhost/phpimagemagic/upload/user20146.jpg', 'http://localhost/phpimagemagic/upload/thumbnail/user20146.jpg', 'http://localhost/phpimagemagic/upload/user20146.', 'http://localhost/phpimagemagic/upload/user20146.', 'f', 'talent', 0, '', 0, 0, '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `temp_users`
--

CREATE TABLE IF NOT EXISTS `temp_users` (
  `confirm_code` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_users`
--

INSERT INTO `temp_users` (`confirm_code`, `username`, `password`, `fname`, `lname`) VALUES
('5e93e73f3d85473c3cfd80807343b4dd', 'bsinghs.007@gmail.com', '2d43bc8af7643171f96713f935ada791', 'Baljeet', 'Singh');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
