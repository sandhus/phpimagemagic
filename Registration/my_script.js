$("#submit").click( function() {
 
if( $("#username").val() == "" || $("#pass").val() == "" )
	  $("#ack").html("Username/Password are mandatory fields -- Please Enter.");
	else
	  $.post( $("#myForm").attr("action"),
	         $("#myForm :input").serializeArray(),
			 function(info) {
 
			   $("#ack").empty();
			   $("#ack").html(info);
				clear();
			 });
 
	$("#myForm").submit( function() {
	   return false;	
	});
});
 
function clear() {
 
	$("#myForm :input").each( function() {
	      $(this).val("");
	});
 
}


$("#login").click( function() {
 
if( $("#username").val() == "" || $("#pass").val() == "" )
	  $("#ack").html("Username/Password are mandatory fields -- Please Enter.");
	else
	  $.post( $("#myFormlogin").attr("action"),
	         $("#myFormlogin :input").serializeArray(),
			 function(info) {
 
			   $("#ack").empty();
			   $("#ack").html(info);
				clear();
			 });
 
	$("#myFormlogin").submit( function() {
	   return false;	
	});
});
 
function clear() {
 
	$("#myFormlogin :input").each( function() {
	      $(this).val("");
	});
 
}